import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CategoryComponent} from './category/category.component';
import {SetCustomerComponent} from './set-customer/set-customer.component';
import {ContactComponent} from './contact/contact.component';

const routes: Routes = [
    {path: 'category/:category/posts', component: CategoryComponent},
    {path: 'customers/set', component: SetCustomerComponent},
    {path: 'contact', component: ContactComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
