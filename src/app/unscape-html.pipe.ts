import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unscapeHtml'
})
export class UnscapeHtmlPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      return value
          .replace(/&amp;/g, '&')
          .replace(/&lt;/g, '<')
          .replace(/&gt;/g, '>')
          .replace(/&quot;/g, '"')
          .replace(/&#039;/g, '\'');
  }

}
