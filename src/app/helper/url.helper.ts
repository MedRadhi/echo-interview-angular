// @ts-ignore
import queryString from 'querystring';
import {CONFIG} from '../config';

export function generate(task: string, vars: {}): string {
    const url = `${CONFIG.baseURL}/?`;
    const params = queryString.stringify({
        token: CONFIG.token,
        task: task,
        vars: JSON.stringify(vars)
    });

    return url + params;
}
