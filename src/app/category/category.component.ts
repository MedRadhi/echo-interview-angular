import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Categories} from '../enum/categories.enum';
import {CategoryService} from '../service/category.service';
import {CategoryResponse} from '../model/category.model';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

    categoryResponse: CategoryResponse;

    constructor(private route: ActivatedRoute, private categoryService: CategoryService) {
    }

    ngOnInit() {
        // Detect route params changes params.
        this.route.params.subscribe(res => {
            // The process of displaying posts of the category should be here.
            const category = this.route.snapshot.paramMap.get('category');

            switch (category) {
                default:
                    this.getCategoryNews();
                    break;
                case Categories.NEWS:
                    this.getCategoryNews();
                    break;
                case Categories.EVENTS:
                    this.getCategoryEvents();
                    break;
                case Categories.MIX:
                    this.getCategoryMix();
                    break;
                case Categories.LIVE_VIDEO:
                    this.getCategoryLiveVideo();
                    break;
            }
        });
    }

    getCategoryNews(): void {
        this.categoryService.getCategoryNews()
            .subscribe(categoryResponse => {
                this.categoryResponse = categoryResponse;
                console.log(this.categoryResponse);
            });
    }

    getCategoryMix(): void {
        this.categoryService.getCategoryMix()
            .subscribe(categoryResponse => this.categoryResponse = categoryResponse);
    }

    getCategoryEvents(): void {
        this.categoryService.getCategoryEvents()
            .subscribe(categoryResponse => this.categoryResponse = categoryResponse);
    }

    getCategoryLiveVideo(): void {
        this.categoryService.getCategoryLiveVideo()
            .subscribe(categoryResponse => this.categoryResponse = categoryResponse);
    }

}
