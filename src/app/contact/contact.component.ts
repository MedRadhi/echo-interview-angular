import {Component, OnInit} from '@angular/core';
import {ContactModel} from '../model/contact.model';
import {ContactService} from '../service/contact.service';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

    contact: ContactModel;

    constructor(private contactService: ContactService) {
    }

    ngOnInit() {
        this.getContact();
    }

    getContact(): void {
        this.contactService.getContact().subscribe(
            contact => this.contact = contact
        );
    }

}
