export enum Categories {
    NEWS = 'getCategoryNews',
    MIX = 'getCategoryMix',
    EVENTS = 'getCategoryEvents',
    LIVE_VIDEO = 'getCategoryLiveVideo'
}
