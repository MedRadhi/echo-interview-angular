import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CategoryComponent} from './category/category.component';
import {HttpClientModule} from '@angular/common/http';
import {UnscapeHtmlPipe} from './unscape-html.pipe';
import {SetCustomerComponent} from './set-customer/set-customer.component';
import {FormsModule} from '@angular/forms';
import { ContactComponent } from './contact/contact.component';

@NgModule({
    declarations: [
        AppComponent,
        CategoryComponent,
        UnscapeHtmlPipe,
        SetCustomerComponent,
        ContactComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
