import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CategoryResponse} from '../model/category.model';
import {generate} from '../helper/url.helper';
import {Categories} from '../enum/categories.enum';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

    constructor(private http: HttpClient) {
    }

    public addCustomer(customer): Observable<{}> {
        return this.http.get<{}>(generate('setNewCustomer', customer));
    }
}
