import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CategoryResponse} from '../model/category.model';
import {generate} from '../helper/url.helper';
import {Categories} from '../enum/categories.enum';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

    public getCategoryNews(): Observable<CategoryResponse> {
        return this.http.get<CategoryResponse>(generate(Categories.NEWS, {page: 1}));
    }

    public getCategoryMix(): Observable<CategoryResponse> {
        return this.http.get<CategoryResponse>(generate(Categories.MIX, {page: 1}));
    }

    public getCategoryEvents(): Observable<CategoryResponse> {
        return this.http.get<CategoryResponse>(generate(Categories.EVENTS, {page: 1}));
    }

    public getCategoryLiveVideo(): Observable<CategoryResponse> {
        return this.http.get<CategoryResponse>(generate(Categories.LIVE_VIDEO, {page: 1}));
    }
}
