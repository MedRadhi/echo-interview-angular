import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {generate} from '../helper/url.helper';
import {ContactModel} from '../model/contact.model';

@Injectable({
    providedIn: 'root'
})
export class ContactService {

    constructor(private http: HttpClient) {
    }

    public getContact(): Observable<ContactModel> {
        return this.http.get<ContactModel>(generate('getContact', {}));
    }
}
