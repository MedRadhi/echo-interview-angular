export interface Post {
    post_ID: number;
    post_title: string;
    post_content: string;
    post_featured: string;
    post_categories: string[];
    custom_soundcloud: string;
    custom_youtube?: any;
    custom_facebook?: any;
    custom_location?: any;
    custom_where: string;
    custom_when: string;
}

export interface CategoryResponse {
    page: number;
    limit: string;
    total: number;
    records: number;
    fields: Post[];
}