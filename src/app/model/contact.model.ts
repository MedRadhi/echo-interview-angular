export interface General {
    contact_address?: any;
    contact_phone?: any;
    contact_fax?: any;
    contact_email?: any;
}

export interface Social {
    facebook?: any;
    youtube?: any;
    instagram?: any;
    soundcloud?: any;
}

export interface ContactModel {
    general: General;
    social: Social;
}