import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetCustomerComponent } from './set-customer.component';

describe('SetCustomerComponent', () => {
  let component: SetCustomerComponent;
  let fixture: ComponentFixture<SetCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
