import {Component, OnInit} from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import {CustomerService} from '../service/customer.service';

@Component({
    selector: 'app-set-customer',
    templateUrl: './set-customer.component.html',
    styleUrls: ['./set-customer.component.css']
})
export class SetCustomerComponent implements OnInit {
    data: any = {};

    public successMessage: string;
    public errorMessage: string;

    constructor(private customerService: CustomerService) {
    }

    ngOnInit() {
    }

    onSubmit(form: NgForm) {
        this.customerService.addCustomer(this.data).subscribe(
            response => {
                if (response === true) {
                    this.successMessage = 'Customer created successfully.';
                    this.errorMessage = null;

                    form.reset();
                } else {
                    this.errorMessage = 'There is an error while adding the customer, check the server.';
                    this.successMessage = null;
                }
            }
        );
    }

}
